// пишет сумму всех чисел в консоль при смене фокуса
(() => {
    let text = document.getElementById("text");
    text.onchange = function () {
        text = this.value;
        const arr = text.split(",");
        const sum = (arr) => {
            let s = 0;
            for (i = 0; i < arr.length; i++) {
                s = s + Number(arr[i]);
            }
            return s;
        }
        console.log(sum(arr));
    }
})();